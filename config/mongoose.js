'use strict'

import mongoose from 'mongoose'
import chalk from 'chalk'
// import winston from './logs'

const host = {
  DEV_LOCAL: 'mongodb://127.0.0.1:27017/pagcam',
  //DEV_LOCAL: 'mongodb://127.0.0.1:27017/socialfriends-mean',
  MLAB: 'mongodb://root:root@ds019688.mlab.com:19688/socialfriends-mean'
}

mongoose.Promise = global.Promise
mongoose.set('useCreateIndex', true)
mongoose.set('useNewUrlParser', true)

// mongoose.connect(host.PRODUCTION, { useNewUrlParser: true })
//   .catch(err => winston.error('[ERROR MONGODB]', err))

mongoose.connect(host.DEV_LOCAL, { useNewUrlParser: true })
  .catch(err => console.log(chalk.red(err)))

export default mongoose