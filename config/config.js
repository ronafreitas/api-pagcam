'use strict'

import mongoose from './mongoose'

const url = {
  localhost: 'http://localhost:5000',
  laptop: 'http://192.168.1.59:3000',
  heroku: 'https://api-pagcam.herokuapp.com/'
}

export default {
  server: {
    port: process.env.PORT || 5000,
    message: `Servidor rodando na porta `
  },
  //SECRET_TOKEN: 'xxxeyabcJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ91685',
  SECRET_TOKEN: '$soJ945AS354S%$_SA54S3$424447DFSsa45njfdnFDS9JJ',
  mongoose,
  url: url.localhost
}
