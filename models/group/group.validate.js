'use strict'

import { Joi, celebrate } from 'celebrate'

const { body, body_pub,params, query, headers } = {
  body: Joi.object().keys({
    _id: Joi.string().alphanum().required(),
    id_user_create: Joi.string().alphanum().required(),
    name: Joi.string().required(),
    avatar: Joi.string().required(),
    description: Joi.string(),
    users: Joi.array(),
    admin: Joi.array(),
  }),
  params: Joi.object({
    id: Joi.string().alphanum()
  }).unknown(),
  query: {

  },
  headers: Joi.object({

  }).unknown()
}

export default {
  BODY: celebrate({ body }),
  PARAMS: celebrate({ params })
}