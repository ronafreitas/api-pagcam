'use strict'

import { Joi, celebrate } from 'celebrate'

const { body, body_pub,params, query, headers } = {
  body: Joi.object().keys({
    //_id: Joi.string().alphanum().required(),
    userId: Joi.string().alphanum().required(),
    groupId: Joi.string().alphanum().required(),
    message: Joi.string().required(),
    filePublication: Joi.array(),
    comment: Joi.array(),
    like: Joi.array()
  }),
  params: Joi.object({
    id: Joi.string().alphanum()
  }).unknown(),
  query: {

  },
  headers: Joi.object({

  }).unknown()
}

export default {
  BODY: celebrate({ body }),
  PARAMS: celebrate({ params })
}