'use strict'

import config from '../../../config/config'

const { Schema } = config.mongoose

const PublicationGroupSchema = new Schema({
    message: String,
    groupId: {
      type: Schema.Types.ObjectId,
      ref: 'Group'
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    totalComment: {
      type: Number,
      default: 0
    },
    totalLike: {
      type: Number,
      default: 0
    },
    filePublication: Array,
    creationDate: Date,
    comment: [{
      type: Schema.Types.ObjectId,
      ref: 'CommentGroup'
    }],
    like: [{
      type: Schema.Types.ObjectId,
      ref: 'LikeGroup'
    }]
  }, { versionKey: false })
  
  export default config.mongoose.model('publicationgroup', PublicationGroupSchema)