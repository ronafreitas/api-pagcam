'use strict'

import config from '../../../config/config'

const { Schema } = config.mongoose

const CommentGroupSchema = new Schema({
  comment: String,
  publicationId: {
    type: Schema.Types.ObjectId,
    ref: 'PublicationGroup'
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'Required field']
  },
  creationDate: Date
}, { versionKey: false })

export default config.mongoose.model('CommentGroup', CommentGroupSchema)