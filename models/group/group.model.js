'use strict'

import config from '../../config/config'

const { Schema } = config.mongoose

const GroupSchema = new Schema({
  id_user_create: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  name: {
    type: String,
    required: true
  },
  avatar: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false,
    default:''
  },
  users: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  admin: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }],
}, { versionKey: false })

export default config.mongoose.model('Group', GroupSchema)