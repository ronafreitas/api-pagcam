'use strict'

import { Joi, celebrate } from 'celebrate'

const { body, params, query, headers } = {
  body: Joi.object().keys({
    _id: Joi.string().required(),
    chats:[{
      creationDate: Joi.date().required(),
      user_id: Joi.string().required(),
      user_avatar: Joi.string().required(),
      user_displayName: Joi.string().required()
    }]
  }),
  params: Joi.object({
    //id: Joi.string().alphanum().required()
    id: Joi.string().required()
  }).unknown(),
  query: {

  },
  headers: Joi.object({

  }).unknown()
}

export default {
  BODY: celebrate({ body }),
  PARAMS: celebrate({ params }),
  BODY_AND_PARAMS: celebrate({ body, params })
}