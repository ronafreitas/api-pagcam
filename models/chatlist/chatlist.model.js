'use strict'

import config from '../../config/config'

const { Schema } = config.mongoose

const ChatlistSchema = new Schema({
  _id:String,
  chats:[{
    creationDate: Date,
    user_id: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: [true, 'Required field']
    },
    user_avatar:String,
    user_displayName:String
  }]
}, { versionKey: false })

export default config.mongoose.model('Chatlist', ChatlistSchema)