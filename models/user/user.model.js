'use strict'

import config from '../../config/config'

const { Schema } = config.mongoose

const defaultImage = 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'

const UserSchema = new Schema({
  displayName: { type: String, required: true },
  avatar: { type: String, default: defaultImage },
  presentation: { type: String, default: '' },
  email: { type: String, required: true, unique: true, lowercase: true },
  username: {
    unique: true,
    type: String,
    required: true,
    lowercase: true,
    minlength: [6, 'Nome de usuário deve conter ao menos 6 caracteres'], maxlength: [16, 'Nome de usuário deve conter até 16 caracteres']
  },
  password: { type: String, required: true },
  signupDate: { type: Date, default: Date.now() },
  lastLogin: Date,
  state: { type: Boolean, default: true },
  online: { type: Boolean, default: false },
  providerId: String
}, { versionKey: false })

export default config.mongoose.model('User', UserSchema) 
