'use strict'

import app from './app'

import http from 'http';
import SocketIO from 'socket.io';

const server = http.Server(app);
const io = new SocketIO(server);

io.on('connection', socket => {
    socket.on('message_to', res => {
        io.emit('emit_me_'+res.idUserTo, res);
    });
})

export default server

