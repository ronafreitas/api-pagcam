'use strict'

import chatController from '../controllers/chat.controller'
import chatValidate from '../models/chat/chat.validate'
import auth from '../auth/auth'

import { errors } from 'celebrate'

export default (app) => {

	app.post('/api/chat', auth.isAuth, chatValidate.BODY, chatController.AddMessage)
	app.get('/api/chat/:id', auth.isAuth,chatValidate.PARAMS, chatController.GetMessageChat)
  	app.use(errors())

}
