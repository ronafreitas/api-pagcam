'use strict'

import likeCommentController from '../../controllers/group/like.controller'
import likeCommentValidate from '../../models/group/like/like.validate'
import auth from '../../auth/auth'

import { errors } from 'celebrate'

export default (app) => {

  app.post('/api/group/unlike', auth.isAuth, likeCommentValidate.BODY, likeCommentController.RemoveLike)
  app.post('/api/group/like', auth.isAuth, likeCommentValidate.BODY, likeCommentController.AddLike)
  app.get('/api/group/like/:id', auth.isAuth, likeCommentValidate.PARAMS, likeCommentController.GetLikeByPublicationId)

  app.use(errors())

}