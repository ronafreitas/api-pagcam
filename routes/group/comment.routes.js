'use strict'

import groupCommentController from '../../controllers/group/comment.controller'
import groupCommentValidate from '../../models/group/comment/comment.validate'
import auth from '../../auth/auth'

import { errors } from 'celebrate'

export default (app) => {

  app.post('/api/group/comment', auth.isAuth, groupCommentValidate.BODY, groupCommentController.AddComment)
  // app.put('/comment', commentController.Updatecomment)
  // app.delete('/comment', commentController.Removecomment)
  //app.get('/api/groupcomment/:id', auth.isAuth,commentValidate.PARAMS, commentController.GetcommentByPublicationId)

  app.use(errors())

}