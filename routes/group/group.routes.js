'use strict'

import groupController from '../../controllers/group/group.controller'
import groupValidate from '../../models/group/group.validate'
import auth from '../../auth/auth'

import { errors } from 'celebrate'

export default (app) => {
  app.post('/api/group', auth.isAuth, groupController.AddGroup)
  app.put('/api/group', auth.isAuth, groupValidate.BODY,groupController.EditGroupById)
  app.get('/api/group', auth.isAuth, groupController.GetGroup)
  app.get('/api/group/:id', auth.isAuth, groupValidate.PARAMS, groupController.GetGroupById)
  app.delete('/api/group/:id', auth.isAuth, groupValidate.PARAMS, groupController.DeleteGroupById)
  app.get('/api/group/name/:name', auth.isAuth, groupValidate.PARAMS, groupController.GetGroupByName)
  app.get('/api/groups/user/:id', auth.isAuth, groupValidate.PARAMS, groupController.GetGroupByUserId)
  app.use(errors())
}