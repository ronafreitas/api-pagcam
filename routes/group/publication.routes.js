'use strict'

import publicationGroupController from '../../controllers/group/publication.controller'
import publicationGroupValidate from '../../models/group/publication/publication.validate'
import auth from '../../auth/auth'

import { errors } from 'celebrate'

export default (app) => {
    app.post('/api/group/publication', auth.isAuth, publicationGroupValidate.BODY,publicationGroupController.AddPublication)
    app.get('/api/group/publications/:id', auth.isAuth,publicationGroupValidate.PARAMS, publicationGroupController.GetPublicationsByGroupId)
    app.use(errors())
}

