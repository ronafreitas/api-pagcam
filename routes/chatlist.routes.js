'use strict'

import chatlistController from '../controllers/chatlist.controller'
import chatlistValidate from '../models/chatlist/chatlist.validate'
import auth from '../auth/auth'

import { errors } from 'celebrate'

export default (app) => {

	app.post('/api/chatlist', auth.isAuth, chatlistValidate.BODY, chatlistController.AddChatlist)
	app.get('/api/chatlist/:id', auth.isAuth,chatlistValidate.PARAMS, chatlistController.GetChatlistByUserId)
  	app.use(errors())

}
