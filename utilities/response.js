'use strict'

export default {
  success: 'OK',
  error: 'ERROR',
  warning: 'WARNING',
  dataAll: 'DATA ALL',
  notAuthorization: 'Você não tem autorização para realizar essa requisição'
}