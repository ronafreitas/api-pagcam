'use strict'

import chatModel from '../models/chat/chat.model'

export default {
  AddMessage: async (req, res) => {
    try {
      req.body.creationDate = Date.now();
      const message = await chatModel.update(
        { _id: req.body._id }, 
        { $push: { "Messages": req.body.Messages  } },
        { upsert: true }
      );

      //const message = await chatModel.insertMany(req.body)
      return res.status(200).send(message)

    }catch(error){
      return res.status(500).send({
        message: `Erro ao executar ${error}`
      })
    }
  },
  GetMessageChat: async (req, res) => {
    try {

      const chatid = req.params.id;
      var page = req.query.page;
      page = parseInt(page);
      var limit = parseInt(page);
      limit = limit+5;
      const response = await chatModel.aggregate([
         {
           $project:
            {
              '_id':chatid,
              Messages: { $slice: [ { $reverseArray: "$Messages" }, page,limit ] },
              //Messages: { $slice: [ "$Messages", page,limit ] },
            }
         }
      ]);
      return res.status(200).send(response[0]);
    }catch(error){
      return res.status(500).send({ error: `Ocorreu um erro, ${error}` })
    }
  },
  GetPublicationByUserId: async (req, res) => {

    let page = req.query.page;
    const limit = 6;

    if(page >= 1){
      page = page - 1;
    }else{
      page = 0;
    }

    try{

      const userId = req.params.userId;
      const publications = await publicationModel
        .find({ userId })
        .limit(limit)
        .skip(page * limit)
        .sort({ creationDate: 'desc' })
        .populate({ path: 'comment', options: { limit: 5, skip: 1, sort: { creationDate: 'desc' }, populate: { path: 'userId', select: ' avatar displayName _id' } } })
        .populate({ path: 'like', populate: { path: 'userId', select: ' avatar displayName _id' }, })

      const total = await publicationModel
        .find({ userId })
        .countDocuments()

      return res.status(200).send({ publications, total })

    }catch(error){
      return res.status(500).send({message: 'Ocorreu um erro'});
    }
  }

}
