'use strict'

import chatlistModel from '../models/chatlist/chatlist.model'

export default {

  AddChatlist: async (req, res) => {
    try {
      var options = { upsert: true, new: true, setDefaultsOnInsert: true };
      var chexts = await chatlistModel.find({
          "_id":req.body._id
      });
      if(chexts.length == 0){
        const chatlist = await chatlistModel.insertMany(req.body,options);
        return res.status(200).send(chatlist);
      }else{
        var chexts = await chatlistModel.find({
            "_id":req.body._id,
            "chats.user_id": req.body.chats.user_id
        });
        if(chexts.length == 0){
          const chatlist = await chatlistModel.update(
            { _id: req.body._id },
            { $push: { chats: req.body.chats  } },
          );
          return res.status(200).send(chatlist);
        }else{
          return res.status(200).send(chexts);
        }
      }
    }catch(error){
      return res.status(500).send({
        message: `Erro ao executar ${error}`
      })
    }
  },
  GetChatlistByUserId: async (req, res) => {
    try {

      const chatid = req.params.id
      const page = req.query.page
      const response = await chatlistModel
        .find({ '_id':chatid })
        //.populate({path: '_id',select: ' Messages'})
        //.limit(5)
        //.skip(page * 5)
        //.sort({ creationDate: 'desc' })

      return res.status(200).send(response)

    } catch (error) {
      return res.status(500).send({ error: `Ocorreu um erro, ${error}` })
    }

  }
  /*,GetPublicationByUserId: async (req, res) => {

    let page = req.query.page
    const limit = 6

    if (page >= 1)
      page = page - 1
    else
      page = 0

    try {

      const userId = req.params.userId

      const publications = await publicationModel
        .find({ userId })
        .limit(limit)
        .skip(page * limit)
        .sort({ creationDate: 'desc' })
        .populate({ path: 'comment', options: { limit: 5, skip: 1, sort: { creationDate: 'desc' }, populate: { path: 'userId', select: ' avatar displayName _id' } } })
        .populate({ path: 'like', populate: { path: 'userId', select: ' avatar displayName _id' }, })

      const total = await publicationModel
        .find({ userId })
        .countDocuments()

      return res.status(200).send({ publications, total })

    } catch (error) {

      return res.status(500).send({
        message: 'Error en el servidor'
      })

    }
  }*/

}
