'use strict'

import publicationModel from '../models/publication/publication.model'
import followerModel from '../models/follower/follower.model'
import likeModel from '../models/like/like.model'
import commentModel from '../models/comment/comment.model'

export default {

  AddPublication: async (req, res) => {
    try{

      req.body.creationDate = Date.now()
      const publication = await publicationModel.insertMany(req.body)
      return res.status(200).send(publication)

    }catch(error){
      return res.status(500).send({
        message: `Erro ao tentar publicar ${error}`
      })
    }
  },
  UpdatePublication: async (req, res) => {

  },
  RemovePublication: async (req, res) => {
    try{
      const publicationId = req.params.publicationId

      const publication = await publicationModel
        .findByIdAndRemove(publicationId)

      const comments = await commentModel
        .find({ publicationId: publicationId }).remove()

      const likes = await likeModel
        .find({ publicationId: publicationId }).remove()

      res.status(200).send({ data: 'Publicação deletada' })

    }catch(err){
      return res.status(500).send(err)
    }
  },
  GetPublicationByUserId: async (req, res) => {

    let page = req.query.page
    const limit = 6

    if(page >= 1){
      page = page - 1;
    }else{
      page = 0;
    }

    try{
      /*{
        "_id" : ObjectId("5c219ee025dc3228586c27c0"),
        "followDate" : ISODate("2018-12-25T01:54:56.442Z"),
        "userId" : ObjectId("5c219e7725dc3228586c27bc"),
        "followerId" : ObjectId("5c218f1025dc3228586c27b9")
      }*/
      const userId = req.params.userId
      let followersId = []
      const response = await followerModel.find({ userId: userId }).select('-_id followerId')
      response.forEach(item => {
        followersId.push(item.followerId)
      });

      const publications = await publicationModel
        .find({$and:[
          {$or:[ {'userId':userId},{'userId':{$in:followersId}} ] },
        ],'local':1})
        .limit(limit).skip(page * limit)
        .sort({ creationDate: 'desc' })
        .populate({ path: 'comment', options: { limit: 10, skip: 0, sort: { creationDate: 'desc' }, populate: { path: 'userId', select: ' avatar displayName _id' } } })
        .populate({ path: 'like', populate: { path: 'userId', select: ' avatar displayName _id' }, })
        .populate({ path: 'userId', select: ' avatar online displayName _id' })

      /*

      import publicationGroupModel from '../models/group/publication/publication.model'

      const publicationsg = await publicationGroupModel
        .find({ 'userId': userId })
        .limit(limit)
        .skip(page * limit)
        .sort({ creationDate: 'desc' })
        .populate({ path: 'comment', options: { limit: 5, skip: 0, sort: { creationDate: 'desc' }, populate: { path: 'userId', select: ' avatar displayName _id' } } })
        .populate({ path: 'like', populate: { path: 'userId', select: ' avatar displayName _id' }, })
        .populate({ path: 'userId', select: ' avatar online displayName _id' })

      publicationsg.forEach(item => {
        publications.push(item);
      })*/
      
      return res.status(200).send({ publications })

      //const total = await publicationModel.find({ userId }).countDocuments()
      //return res.status(200).send({ publications, total })
    }catch(error){
      return res.status(500).send(error)
    }
  },
  GetPublicationFollowersByUserId: async (req, res) => {

      /*const publications = await publicationModel
        .aggregate([{
          $lookup: {
            from: "publicationgroups",
            localField: "userId",
            foreignField: "userId",
            as: "combina"
          }
        }])*/


    let page = req.query.page
    const limit = 6

    if (page >= 1)
      page = page - 1
    else
      page = 0

    try{
      const userId = req.params.userId
      let followersId = []

      const response = await followerModel.find({ userId: userId })

      response.forEach(item => {
        followersId.push(item.followerId)
      })

      const publications = await publicationModel
        .find({ userId: { $in: followersId } })
        .limit(limit).skip(page * limit)
        .sort({ creationDate: 'desc' })
        .populate({ path: 'comment', options: { limit: 5, skip: 0, sort: { creationDate: 'desc' }, populate: { path: 'userId', select: ' avatar displayName _id' } } })
        .populate({ path: 'like', populate: { path: 'userId', select: ' avatar displayName _id' }, })
        .populate({ path: 'userId', select: ' avatar displayName _id' })

      const total = await publicationModel
        .find({ userId: followersId })
        .countDocuments()

      return res.status(200).send({ publications, total })
    }catch(error){
      return res.status(500).send({
        message: 'Erro ao tentar pesquisar'
      })
    }
  },
  GetPhotosByUserId: async (req, res) => {
    try{
      const userId = req.params.userId
      const publications = await publicationModel.find({ userId,filePublication: {$exists: true, $not: {$size: 0}} }).select('filePublication.url')
      const total = await publicationModel.find({ userId,filePublication: {$exists: true, $not: {$size: 0}} }).select('filePublication.url').countDocuments()

      return res.status(200).send({ publications, total })

    }catch(error){
      return res.status(500).send({
        message: 'Erro ao tentar pesquisar'
      })
    }
  }

}
