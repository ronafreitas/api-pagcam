'use strict'

import groupCommentModel from '../../models/group/comment/comment.model'
import publicationGroupModel from '../../models/group/publication/publication.model'

export default {
  AddComment: async (req, res) => {
    try{
      req.body.creationDate = Date.now()
      const response = await groupCommentModel.insertMany(req.body)
      const { publicationId } = req.body

      const x = await publicationGroupModel.findOneAndUpdate(
        { _id: publicationId },
        {
          $push: { comment: response[0] },
          $inc: { totalComment: 1 }
        }
        , { new: true })

      return res.status(200).send(response[0])
    }catch(error){
      return res.status(500).send({ error: `Ocorreu um erro, ${error}` })
    }
  },
  GetcommentByPublicationId: async (req, res) => {
    try{
      const publicationId = req.params.id
      const page = req.query.page
      const response = await groupCommentModel
        .find({ publicationId })
        .populate({
          path: 'userId',
          select: ' avatar displayName _id'
        })
        .limit(5)
        .skip(page * 5)
        .sort({ creationDate: 'desc' })

      return res.status(200).send(response)
    }catch(error){
      return res.status(500).send({ error: `Ocorreu um erro, ${error}` })
    }
  },
  DeleteCommentById: () => {

  },
  UpdateCommentById: () => {

  }

}