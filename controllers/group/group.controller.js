'use strict'

import groupModel from '../../models/group/group.model'

export default {

  AddGroup: async (req, res) => {
    try{
      const group = await groupModel.insertMany(req.body)
      res.status(200).send(group)
    }catch(err){
      res.status(404).send({
        error: `Ocorreu um erro: ${error}`
      })
    }
  },
  GetGroup: async (req, res) => {

    let page = req.query.page
    const limit = 6

    if (page >= 1)
      page = page - 1
    else
      page = 0

    try{
      const groups = await groupModel
        .find()
        .limit(limit)
        .skip(page * limit)
        
      return res.status(200).send(groups)

    }catch(error){
      return res.status(500).send({
        message: `Ocorreu um erro: ${error}`
      })
    }
  },
  GetGroupById: async (req, res) => {
    try{
      const id = req.params.id;
      const groups = await groupModel
        .find({ _id: id })
        .select('-admin -description -users -id_user_create')

      return res.status(200).send(groups)
    }catch(error){
      return res.status(500).send({})
    }
  },
  GetGroupByUserId: async (req, res) => {
    try{
      const id = req.params.id;
      const groups = await groupModel
        .find({ id_user_create: id })
        .select('-admin -description -users -id_user_create')
      return res.status(200).send(groups)
    }catch(error){
      return res.status(500).send({})
    }
  },
  DeleteGroupById: async (req, res) => {
    try{
      const id = req.params.id;
      const delgroup = await groupModel.find({ _id: id }).remove()




      /*





                    REMOVER AS PUBLICAÇÕES DO GRUPO




                    

      */




      return res.status(200).send(delgroup)
    }catch(error){
      return res.status(500).send({})
    }
  },
  EditGroupById: async (req, res) => {
    try {
      //req.body.creationDate = Date.now();
      //const message = await groupModel.update(
        //{ _id: req.body.id }, req.body, { multi: false }
      const upgroup = await groupModel.findByIdAndUpdate(
        req.body._id,
        req.body,
        { multi: false }
      )
      return res.status(200).send(upgroup)
    }catch(error){
      return res.status(500).send({
        message: `Erro ao executar ${error}`
      })
    }
  },
  GetGroupByName: async (req, res) => {
    try{
      const groupname = req.params.name;
      const group = await groupModel
        .find({'name' : new RegExp(groupname, 'i')})
        .select('-admin -description -users -id_user_create')
      return res.status(200).send(group)
    }catch(error){
      return res.status(500).send({
        message: `Erro ao executar ${error}`
      })
    }
  }
}