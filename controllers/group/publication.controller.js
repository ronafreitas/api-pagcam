'use strict'

import publicationGroupModel from '../../models/group/publication/publication.model'

export default {
  AddPublication: async (req, res) => {
    try{
      const pubgroup = await publicationGroupModel.insertMany(req.body)
      res.status(200).send(pubgroup)
    }catch(err){
      res.status(404).send({
        error: `Ocorreu um erro: ${error}`
      })
    }
  },
  GetPublicationsByGroupId: async (req, res) => {
    let page = req.query.page;
    const limit = 6;

    if(page >= 1){
      page = page - 1;
    }else{
      page = 0;
    }

    try{
      const groupId = req.params.id;
      const publications = await publicationGroupModel
        .find({ groupId })
        .limit(limit)
        .skip(page * limit)
        .sort({ creationDate: 'desc' })
        .populate({ path: 'comment', options: { limit: 5, skip: 0, sort: { creationDate: 'desc' }, populate: { path: 'userId', select: ' avatar displayName _id' } } })
        .populate({ path: 'like', populate: { path: 'userId', select: ' avatar displayName _id' }, })
        .populate({ path: 'userId', select: ' avatar online displayName _id' })

      const total = await publicationGroupModel
        .find({ groupId })
        .countDocuments()
      return res.status(200).send({ publications, total })
    }catch(error){
      return res.status(500).send(error);
    }
  },
}