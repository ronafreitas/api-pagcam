'use strict'

import userModel from '../models/user/user.model'
import utilities from '../utilities/utilities'

export default {

  GetUsers: async (req, res) => {

    const limit = 6
    let findProperty = {}

    let displayname = req.query.displayname
    let page = req.query.page

    if (page >= 1)
      page = page - 1
    else
      page = 0

    try{

      if(displayname != null && displayname != 'undefined'){
        findProperty = { displayName: new RegExp(displayname, 'i') }
      }

      const users = await userModel
        .find(findProperty)
        .select(['-password'])
        .limit(limit)
        .skip(page * limit)
        .sort('-signupDate')

      const total = await userModel
        .find()
        .countDocuments()

      return res.status(200).send({ users, total })
    }catch(error){
      return res.status(500).send({
        message: `Ocorreu um erro: ${error}`
      })
    }
  },
  UpdateUser: async (req, res) => {
    try{
      const userId = req.params.id
      const response = await userModel.findByIdAndUpdate(userId, { $set: req.body }, { new: true })
      return res.status(200).send(response)
    }catch(err){
      return res.status(500).send({ err })
    }
  },

  // UpdateAvatar: async (req, res) => {

  //   try {

  //     const { userId, avatar_url } = req.body

  //     const response = await userModel
  //       .findByIdAndUpdate(userId, { avatar: avatar_url }, { new: true })

  //     return res.status(200).send(response)

  //   } catch (err) {
  //     return res.status(500).send({ err })
  //   }

  // },

  UpdateUsersOnlyPassword: async (req, res) => {
    try{
      //console.log( req.body.id ); 
      const userId = req.params.id
      //const response = await userModel.findById(userId)

      const response = await userModel
      .findByIdAndUpdate(userId,
        { password: utilities.EncodePassword(req.body.password) },
        { new: true })

      return res.status(200).send(response)

      /*const { actual_password, new_password } = req.body;
      const userId = req.params.id;
      const { password } = await userModel.findById(userId);
      if(utilities.DecodePassword(actual_password, password)){

        const response = await userModel
          .findByIdAndUpdate(userId,
            { password: utilities.EncodePassword(new_password) },
            { new: true })

        return res.status(200).send({ message: 'Senha alterada' })
      }else{
        return res.status(200).send({ message: 'Senha incorreta' })        
      }*/
      //return res.status(200).send({ message: req.body })
    }catch(err){
      console.log('bbbbb');
      return res.status(500).send({ err })
    }
  },
  DeactivateAccount: async (req, res) => {
    try{
      //console.log( req.body.id ); 
      const userId = req.params.id
      //const response = await userModel.findById(userId)

      const response = await userModel.findByIdAndUpdate(userId,{state : false,online : false})

      return res.status(200).send(response)

      /*const { actual_password, new_password } = req.body;
      const userId = req.params.id;
      const { password } = await userModel.findById(userId);
      if(utilities.DecodePassword(actual_password, password)){

        const response = await userModel
          .findByIdAndUpdate(userId,
            { password: utilities.EncodePassword(new_password) },
            { new: true })

        return res.status(200).send({ message: 'Senha alterada' })
      }else{
        return res.status(200).send({ message: 'Senha incorreta' })        
      }*/
      //return res.status(200).send({ message: req.body })
    }catch(err){
      console.log('bbbbb');
      return res.status(500).send({ err })
    }
  },
  GetUserById: async (req, res) => {
    try{
      const id = req.params.id;
      const user = await userModel
        .findOne({ _id: id })
        .select(['-password'])

      if(user === null){
        return res.status(200).send({ data: 'Usuário não encontrado' })
      }else{
        return res.status(200).send(user)
      }
    }catch(error){
      return res.status(500).send({})
    }
  },
  GetUserByUsername: async (req, res) => {
    try{
      const username = req.params.name;
      const user = await userModel
        .findOne({'username' : new RegExp(username, 'i')})
        .select('-password -presentation -signupDate -email -providerId')
      return res.status(200).send(user)
    }catch(error){
      return res.status(500).send({})
    }
  }

}

